$(function(){
	
	$("#onmenu").on('click', function() {
  	$("nav").toggleClass("on");
  	$(".filter").toggleClass("m-filter");
  	$(".m-filter").on('click', function() {
  		$("nav").removeClass("on");
  		$(".filter").removeClass("m-filter");
  	});
	});

	var parent, ink, d, x, y;
	$("li a").on("click", function(e) {
  	parent = $(this).parent();
  	if(parent.find(".ink").length == 0)
    	parent.prepend("<span class='ink'></span>");
 
  	ink = parent.find(".ink");
  	ink.removeClass("animate");
 		
  	if(!ink.height() && !ink.width())
  	{
    	d = Math.max(parent.outerWidth(), parent.outerHeight());
    	ink.css({height: d, width: d});
		}
 
  	x = e.pageX - parent.offset().left - ink.width()/2;
  	y = e.pageY - parent.offset().top - ink.height()/2;
 		
  	ink.css({top: y+'px', left: x+'px'}).addClass("animate");
  	setTimeout(function(){
        ink.css({height: '', width: '', top: '', left: ''}).removeClass("animate");
    },700);
	});

	function open_close(val) {
		val.forEach(function(value) {
			$(value)
				.removeClass("open-contents")
				.addClass("close-contents");
		});
	};

	var contents = ["#top", "#health", "#Warking", "#analysis", "#student"];

	$(".change").on("click",function() {
		//console.log(typeof($(this).data('type')));
		switch ($(this).data('type')) {
			case "t":
				open_close(contents);
				$(contents[0]).toggleClass("open-contents")
				break;
			case "h":
				open_close(contents);
				$(contents[1]).toggleClass("open-contents")
				break;
			case "w":
				open_close(contents);
				$(contents[2]).toggleClass("open-contents")
				break;
			case "a":
				open_close(contents);
				$(contents[3]).toggleClass("open-contents")
				break;
			case "s":
				open_close(contents);
				$(contents[4]).toggleClass("open-contents")
				break;
		}
	});

	$("#pagetop").click(function(){
		$("html,body").animate({scrollTop:0},'slow');
		return false;
	});

});